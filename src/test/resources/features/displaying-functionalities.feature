
Feature: Displaying of Top Rated Movies functionalities

  Scenario: Successful verifying that first 10 movies have rating greater than 8 in Top Rated Movies section
    Given open website imdb.com
    When go to Menu
    And go to Top Rated Movies section
    Then first 10 movies should have rating greater than 8