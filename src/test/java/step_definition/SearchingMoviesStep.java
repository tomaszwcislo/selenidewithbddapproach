package step_definition;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static org.openqa.selenium.By.cssSelector;

public class SearchingMoviesStep {
    @Given("open website imdb.com")
    public void openWebsiteImdbCom() {
        Configuration.reportsFolder = "target/surefire-reports";
        open("https://www.imdb.com/");
    }

    @When("a keyword {string} is entered in input field")
    public void aKeywordIsEnteredInInputField(String arg0) {
        $(By.name("q")).setValue("Rocky").pressEnter();
    }

    @Then("list of films should be shown")
    public void listOfFilmsShouldBeShown() {
        $(cssSelector("span.findSearchTerm")).shouldHave(Condition.exactText("\"Rocky\""));
    }
}
