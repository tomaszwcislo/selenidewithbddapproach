package step_definition;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static org.assertj.core.api.Assertions.assertThat;
import static org.openqa.selenium.By.cssSelector;

public class DisplayingTestStep {
    @When("go to Menu")
    public void goToMenu() {
        $(cssSelector("div.ipc-button__text")).shouldBe(visible).click();
    }

    @And("go to Top Rated Movies section")
    public void goToTopRatedMoviesSection() {
        $(cssSelector("a[href='/chart/top/?ref_=nv_mv_250']")).shouldBe(visible).click();
        $(cssSelector("h1")).shouldBe(visible).shouldHave(exactText("Top Rated Movies"));
    }

    @Then("first {int} movies should have rating greater than {int}")
    public void firstMoviesShouldHaveRatingGreaterThan(int arg0, int arg1) {
        $$(cssSelector(".ratingColumn.imdbRating"))
                .first(10).shouldHaveSize(10).texts()
                .forEach(strong-> assertThat(Double.parseDouble(strong.replace(",", "."))).isGreaterThan(8));
    }
}
